<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('signup', 'AuthController@signup');
Route::post('signin', 'AuthController@signin');

Route::group(['middleware' => 'jwt.verify'], function () {
    Route::post('logout', 'AuthController@logout');

    Route::get('goals', 'GoalController@index');
    Route::get('goals/{id}', 'GoalController@get');
    Route::post('create', 'GoalController@create');
    Route::post('update', 'GoalController@update');
    Route::post('delete', 'GoalController@delete');
});