# New Year Goals

`1. Clone the repo`

`2. Rename .env.example to .env`

`3. Set DB, check for more information here https://laravel.com/docs/8.x/database`

`4. Set JWT token`

`5. Install all dependenices by running composer install && npm install`

`6. Run php artisan serve`

`7. Run php artisan migrate`

Enjoy :P