import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { BrowserRouter } from 'react-router-dom'

import Header from './components/layout/Header';
import Body from './components/layout/Body';

class Index extends Component {
  render () {
    return (
        <div id="wrapper">
            <BrowserRouter>
                <Header />
                <Body />
            </BrowserRouter>
      </div>
    )
  }
}

ReactDOM.render(<Index />, document.getElementById('index'));