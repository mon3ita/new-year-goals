import axios from 'axios';

const axios_instance = axios.create({
    baseURL: '/api'
});

function getUser() {
    const token = localStorage.getItem("token");
    const name = localStorage.getItem("name") || "Guest";

    return [token, name];
}

function setGoalImportanceClass(importance, done) {
    let class_ = "";

    console.log(done);
    if(done == 1) {

        class_ = "alert alert-secondary";
        return class_;
    }

    if(importance == "important")
        class_ = "alert alert-success";
    else if(importance == "not so important")
        class_ = "alert alert-primary";
    else
        class_ = "alert alert-danger";

    return class_;
}

function getDate(date) {
    return `${date.getDate()} / ${date.getMonth()} / ${date.getFullYear()}`;
}

export { axios_instance, getUser, setGoalImportanceClass, getDate };