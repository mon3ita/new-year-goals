import React, { Component } from 'react';

import { axios_instance } from '../../utils';

export default class SignIn extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: "",
            password: "",
            errors: []
        };

        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.login = this.login.bind(this);
    }

    handleEmailChange(e) {
        this.setState({ email: e.target.value });
    }

    handlePasswordChange(e) {
        this.setState({ password: e.target.value });
    }

    login(e) {
        const errors = [];

        const user = this.state;
        if(!user.email)
            errors.push("Email must be provided.");

        if(!user.password)
            errors.push("Password must be provided.")


        if(!errors.length) {
            axios_instance.post('/signin', user)
                .then(response => {
                    if(response.data.error) {
                        errors.push(response.data.error);
                        this.setState({ errors });
                    } else {
                        const _resp = JSON.parse(response.data.data);
                        localStorage.setItem("token", _resp.token);
                        localStorage.setItem("name", _resp.name);
                        window.location.href = "/";
                    }

                }).catch(err => {
                    errors.push("Invalid credentials.");
                    this.setState({ errors });
                });
        } else
            this.setState({ errors });

        e.preventDefault();
    }

    render() {
        const user = this.state;
        const errors = this.state.errors.map((err, idx) => 
            <div className="alert alert-danger">
                {err}
            </div>);

        return(
            <form className="form-control">
                <h1>Sign In</h1>
                <hr/>
                {this.state.errors.length ? <div>{errors}<hr/></div> : ""}
                <div className="form-group">
                    <input type="email" onChange={ (e) => this.handleEmailChange(e) } value={user.email} className="form-control" placeholder="Email" />
                </div>

                <div className="form-group">
                    <input type="password" onChange={ (e) => this.handlePasswordChange(e) } value={user.password} className="form-control" placeholder="Password" />
                </div>

                <button onClick={(e) => this.login(e) } className="btn btn-success">Login</button>
            </form>
        );
    }
}