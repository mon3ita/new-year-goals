import React, { Component } from 'react';

import { axios_instance } from '../../utils';

export default class SignUp extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            name: "",
            email: "",
            password: "",
            password_confirmation: "",
            errors: []
        };


        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handlePasswordConfirmationChange = this.handlePasswordConfirmationChange.bind(this);
        this.handleRegistration = this.handleRegistration.bind(this);
    }

    handleNameChange(e) {
        this.setState({ name: e.target.value });
    }

    handleEmailChange(e) {
        this.setState({ email: e.target.value });
    }

    handlePasswordChange(e) {
        this.setState({ password: e.target.value });
    }

    handlePasswordConfirmationChange(e) {
        this.setState({ password_confirmation: e.target.value });
    }

    handleRegistration(e) {
        const errors = [];

        const user = this.state;

        if(!user.name) {
            errors.push("Name must be provided.");
        }

        if(!user.email) {
            errors.push("Email must be provided.");
        }

        if(!user.password) {
            errors.push("Password must be provided");
        }

        if(user.password && user.password.length < 8) {
            errors.push("Password must be at least 8 chars long.");
        }

        if(!errors.length) {
            axios_instance.post('/signup', user)
                .then(response => {
                    window.location.href = "/signin";
                }).catch((err) => {
                    errors.push("User with given email already exists.");
                    this.setState({ errors });
            });
        } else
            this.setState({ errors });
        e.preventDefault();
    }

    render() {
        const user = this.state;

        const errors = this.state.errors.map((err, idx) => 
            <div className="alert alert-danger">
                {err}
            </div>);

        return(
            <form className="form-control" method="post">
                <h1>Sign Up</h1>
                <hr/>
                {this.state.errors.length ? <div>{errors}<hr/></div> : ""}
                <div className="form-group">
                    <input type="text" onChange={(e) => this.handleNameChange(e) } value={user.name} className="form-control" placeholder="Name" />
                </div>

                <div className="form-group">
                    <input type="email" onChange={(e) => this.handleEmailChange(e) } value={user.email} className="form-control" placeholder="Email" />
                </div>

                <div className="form-group">
                    <input type="password" onChange={(e) => this.handlePasswordChange(e) } value={user.password} className="form-control" placeholder="Password" />
                </div>

                <div className="form-group">
                    <input type="password" onChange={(e) => this.handlePasswordConfirmationChange(e) } value={user.password_confirmation} className="form-control" placeholder="Password Confirmation" />
                </div>

                <button className="btn btn-success" onClick={ (e) => this.handleRegistration(e) }>Register</button>
            </form>
        );
    }
}