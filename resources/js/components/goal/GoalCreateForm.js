import React, { Component } from 'react';

import { axios_instance, getUser } from "../../utils";

export default class GoalCreateForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "",
            description: "",
            importance_options: ["not so important", "important", "really important"],
            importance: "not so important",
            errors: []
        };

        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleImportanceChange = this.handleImportanceChange.bind(this);
        this.handleCreateGoal = this.handleCreateGoal.bind(this);
    }

    handleTitleChange(e) {
        this.setState({ title: e.target.value });
    }

    handleDescriptionChange(e) {
        this.setState({ description: e.target.value });
    }

    handleImportanceChange(e) {
        this.setState({ importance: e.target.value})
    }

    handleCreateGoal(e) {
        const errors = [];
        const goal = this.state;

        const token = getUser()[0];
        const headers = {
            "Authorization": `Bearer ${token}`
        }

        if(!goal.title)
            errors.push("Title must be provided.");

        if(!goal.description)
            errors.push("Description must be provided.");


        if(!errors.length) {
            axios_instance.post("/create", goal, {
                headers: headers
                }).then(repsonse => {
                    if(repsonse.data.errors) {
                        for(var err of repsonse.data.errors)
                            errors.push(err.message);
                        this.setState({ errors });
                    } else {
                        window.location.href = "/goals";
                    }
                }).catch(err => {
                    errors.push(err);
                    this.setState({ errors });
            });
        } else
            this.setState({ errors });

        e.preventDefault();
    }

    render() {
        const goal = this.state;

        const errors = this.state.errors.map((err, idx) => 
            <div className="alert alert-danger">
                {err}
            </div>);

        const options = this.state.importance_options.map((opt, idx) => 
        <option key={idx}>{opt}</option> );

        return(
            <form className="form-control">
                <h1>Create Goal</h1>
                <hr/>
                {this.state.errors.length ? <div>{errors}<hr/></div> : ""}
                <div className="form-group">
                    <input type="text" onChange={(e) => this.handleTitleChange(e)} value={goal.title} className="form-control" placeholder="Title" />
                </div>

                <div className="form-group">
                    <textarea rows='10' onChange={(e) => this.handleDescriptionChange(e) } value={goal.description} className="form-control" placeholder="Description" />
                </div>

                <div className="form-group">
                    <select className="form-control" onChange={(e) => this.handleImportanceChange(e) } >
                        {options}
                    </select>
                </div>

                <button onClick={(e) => this.handleCreateGoal(e)} className="btn btn-success">Create</button>
            </form>
        );
    }
}