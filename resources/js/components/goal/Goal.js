import React, { Component } from 'react';

import { Link } from "react-router-dom";

import { axios_instance, getUser } from "../../utils";

export default class Goal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            goal: {}
        }
    }

    componentDidMount() {
        const id = this.props.match.params.id;

        const token = getUser()[0];
        if(!token)
            window.location.href = "/signin";
        
        const headers = {
            "Authorization": `Bearer ${token}`
        };

        axios_instance.get(`/goals/${id}`, {
            headers: headers
        }).then(response => {
            this.setState({ goal: response.data[0] });
        }).catch(err => {
            console.log(err);
        })
    }

    render() {
        const goal = this.state.goal;
        const color = goal.importance == "important" ? "green" : (goal.importance == "really important" ?
            "red" : "blue"
        );

        return(
            <div>
                <h2 style={{color: `${color}`}}>{goal.title} <Link className="btn btn-primary btn-sm" to={`/goals/${goal.id}/update`}>Update</Link></h2>
                <hr/>
                <div>
                    {goal.description}
                </div>
            </div>
        );
    }
}