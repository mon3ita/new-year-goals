import React, { Component } from 'react';

import { axios_instance, getUser } from "../../utils";

export default class GoalUpdateForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: "",
            title: "",
            description: "",
            importance_options: ["not so important", "important", "really important"],
            importance: "",
            achieved: 0,
            errors: []
        };

        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleImportanceChange = this.handleImportanceChange.bind(this);
        this.handleUpdateGoal = this.handleUpdateGoal.bind(this);
        this.handleDoneChange = this.handleDoneChange.bind(this);
    }

    componentDidMount() {
        const token = getUser()[0];
        const id = this.props.match.params.id;

        const headers = {
            "Authorization": `Bearer ${token}`
        };

        axios_instance.get(`/goals/${id}`, {
            headers: headers
        }).then(response => {
            this.setState({
                id: id,
                title: response.data[0].title,
                description: response.data[0].description,
                importance: response.data[0].importance,
                achieved: Number(response.data[0].achieved),
            });
        }).catch(err => {
            console.log(err);
        });
    }

    handleTitleChange(e) {
        this.setState({ title: e.target.value });
    }

    handleDescriptionChange(e) {
        this.setState({ description: e.target.value });
    }

    handleImportanceChange(e) {
        this.setState({ importance: e.target.value})
    }

    handleUpdateGoal(e) {
        const errors = [];
        const goal = this.state;

        const token = getUser()[0];
        const headers = {
            "Authorization": `Bearer ${token}`
        };

        if(!goal.title)
            errors.push("Title must be provided.");

        if(!goal.description)
            errors.push("Description must be provided.");


        if(!errors.length) {
            axios_instance.post("/update", goal, {
                headers: headers
                }).then(response => {
                    if(response.data.errors) {
                        for(var err of response.data.errors)
                            errors.push(err.message);
                        this.setState({ errors });
                    } else {
                       window.location.href = "/goals";
                    }
                }).catch(err => {
                    errors.push(err);
                    this.setState({ errors });
                });
        } else
            this.setState({ errors });

        e.preventDefault();
    }

    handleDoneChange() {
        this.setState({ achieved: Math.abs(this.state.achieved - 1) });
    }

    render() {
        const goal = this.state;

        const errors = this.state.errors.map((err, idx) => 
            <div className="alert alert-danger">
                {err}
            </div>);

        const options = this.state.importance_options.map((opt, idx) => 
            <option key={idx}>{opt}</option>
        );

        return(
            <form className="form-control">
                <h1>Update Goal</h1>
                <hr/>
                {this.state.errors.length ? <div>{errors}<hr/></div> : ""}
                <div className="form-group">
                    <input type="text" onChange={(e) => this.handleTitleChange(e)} value={goal.title} className="form-control" placeholder="Title" />
                </div>

                <div className="form-group">
                    <textarea rows='10' onChange={(e) => this.handleDescriptionChange(e) } value={goal.description} className="form-control" placeholder="Description" />
                </div>

                <div className="form-group">
                    <select className="form-control" onChange={(e) => this.handleImportanceChange(e) } >
                        {options}
                    </select>
                </div>

                <div className="form-group">
                    Achieved: <input type="checkbox" onChange={this.handleDoneChange}  checked={(goal.achieved == 1)} />
                </div>

                <button onClick={(e) => this.handleUpdateGoal(e)} className="btn btn-success">Update</button>
            </form>
        );
    }
}