import React, { Component } from 'react';

import { Link } from "react-router-dom";

import { axios_instance, getUser, setGoalImportanceClass, getDate } from "../../utils";

export default class GoalList extends Component {
    constructor(props) {

        super(props);
        this.state = {
            goals: [],
            filtered: [],
            filter_by: ""
        };

        this.handleFilterChange = this.handleFilterChange.bind(this);
        this.handleDeleteGoal = this.handleDeleteGoal.bind(this);
    }

    componentDidMount() {
        const token = getUser()[0];

        if(!token)
            window.location.href = "/signin";
        
        const headers = {
            "Authorization": `Bearer ${token}`
        };

        axios_instance.get("/goals", {
            headers: headers
        }).then(response => {
            this.setState({ goals: response.data.data, filtered: response.data.data });
        }).catch(err => {
            console.log(err);
        });
    }

    handleFilterChange(e) {
        const filter_by = e.target.value;
        let filtered = this.state.goals;

        if(filter_by != "Filter by") {
            filtered = this.state.goals.filter((goal) => goal.importance == filter_by);
        } 

        this.setState({ filtered });
        this.setState({ filter_by: e.target.value });
    }

    handleDeleteGoal(e, id) {

        const token = getUser()[0];
        if(!token)
            window.location.href = "/signin";
        
        const headers = {
            "Authorization": `Bearer ${token}`
        };

        const message = "";

        const data = {
            id: id
        };

        axios_instance.post(`/delete`, data, {
            headers: headers
        }).then(response => {
            this.setState({ goals: response.data.goals});
            const filter_by = this.state.filter_by;

            let filtered;
            if(filter_by != "" && filter_by != "Filter by") {
                filtered = response.data.goals.filter((goal) => goal.importance == filter_by);
            } else {
                filtered = response.data.goals;
            }

            this.setState({ filtered });
        }).catch(err => {
            console.log(err);
        })

        e.preventDefault();
    }

    render() {
        const goals = this.state.filtered.map((goal, idx) =>
            <div className={setGoalImportanceClass(goal.importance, goal.achieved)}>
                <Link to={`/goals/${goal.id}`}>{goal.title}
                </Link>&nbsp;&nbsp;
                <Link className="btn btn-primary btn-sm" to={`/goals/${goal.id}/update`}>Edit</Link>
                &nbsp;&nbsp;
                <Link className="btn btn-danger btn-sm" onClick={(e) => this.handleDeleteGoal(e, goal.id) } >X</Link>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style={{float: "right"}}>Created At: {getDate(new Date(goal.created_at))} | Updated: {getDate(new Date(goal.updated_at))}</span>
            </div>
        );

        const options = ["not so important", "important", "really important"].map((opt, idx) => 
            <option key={idx}>{opt}</option>
        )

        return(
            <div>
                <select className="form-select" onChange={(e) => this.handleFilterChange(e) } >
                    <option key="Filter by" default>Filter by</option>
                    {options}
                </select>
                <hr/>
                {goals}
            </div>
        );
    }
}