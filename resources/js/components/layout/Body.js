import React, { Component } from 'react';

import { Switch, Route, Link } from "react-router-dom";

import Home from '../home/Home';

import GoalCreateForm from "../goal/GoalCreateForm";
import Goal from "../goal/Goal";
import GoalUpdateForm from "../goal/GoalUpdateForm";
import GoalList from "../goal/GoalList";

import SignIn from "../user/SignIn";
import SignUp from "../user/SignUp";

import { getUser } from "../../utils";

export default class Body extends Component{

    constructor(props) {
        super(props);

        this.logout = this.logout.bind(this);
    }

    logout() {
        localStorage.removeItem("token");
        localStorage.removeItem("name");
        window.location.href = "/signin";
    }

    render() {
        const user = getUser()[1];

        return(
            <div id="content-wrapper" className="d-flex flex-column">
                <div id="content">
                    <nav className="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
                         <ul className="navbar-nav ml-auto">
                            <li className="nav-item dropdown no-arrow">
                                {user == "Guest" ? 
                                    <h5>
                                        <Link to="/signin">Sign In</Link>&nbsp;|&nbsp;
                                        <Link to="/signup">Sign Up</Link>
                                    </h5> : <Link to="/logout" onClick={this.logout}><h5>Log Out</h5></Link>
                                }
                            </li>

                         </ul>
                    </nav>
                    <div className="container-fluid" style={{padding: "30px"}}>
                        <Switch>
                            <Route exact path="/" component={Home} />

                            <Route exact path="/goals" component={GoalList} />
                            <Route exact path="/goals/create" component={GoalCreateForm} />
                            <Route exact path="/goals/new" component={GoalList} />
                            <Route exact path="/goals/achieved" component={GoalList} />
                            <Route exact path="/goals/progress" component={GoalList} />
                            
                            <Route exact path="/goals/:id" component={Goal} />
                            <Route exact path="/goals/:id/update" component={GoalUpdateForm} />
                        
                            <Route exact path="/signin" component={SignIn} />
                            <Route exact path="/signup" component={SignUp} />
                        </Switch>
                    </div>
                </div>
            </div>
        );
    }
}