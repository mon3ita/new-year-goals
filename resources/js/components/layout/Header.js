import React, { Component } from 'react';

import { Link } from "react-router-dom";

import { getUser } from "../../utils";

export default class Header extends Component {

    render() {

        const user = getUser()[1];

        return(
            <div>
            {user != "Guest" ? <ul className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
                <Link className="sidebar-brand d-flex align-items-center justify-content-center" to="/">
                    <div className="sidebar-brand-icon rotate-n-15">
                        <i className="fas fa-laugh-wink"></i>
                    </div>
                    <div className="sidebar-brand-text mx-3">NewYear Goals</div>
                </Link>

                <hr className="sidebar-divider my-0" />

                <li className="nav-item active">
                    <Link className="nav-link" to="/">
                        <i className="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span></Link>
                </li>

                <hr className="sidebar-divider" />

                <div className="sidebar-heading">
                    My Goals
                </div>

                <hr className="sidebar-divider" />

                <li className="nav-item">
                    <Link className="sidebar-card" to="/goals/create">
                        <span>Create</span>
                    </Link>
                </li>

                <li className="nav-item">
                    <Link className="nav-link collapsed" to="/goals">
                        <i className="fas fa-fw fa-cog"></i>
                        <span>All</span>
                    </Link>
                </li>
                <hr className="sidebar-divider" />

                <div className="sidebar-heading">
                    Past Goals
                </div>

                <hr className="sidebar-divider d-none d-md-block" />

                <div className="text-center d-none d-md-inline">
                    <button className="rounded-circle border-0" id="sidebarToggle"></button>
                </div>

                <div className="sidebar-card">
                    Hello, {user}
                </div>

            </ul> : <ul className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
                <Link className="sidebar-brand d-flex align-items-center justify-content-center" to="/">
                    <div className="sidebar-brand-icon rotate-n-15">
                        <i className="fas fa-laugh-wink"></i>
                    </div>
                    <div className="sidebar-brand-text mx-3">NewYear Goals</div>
                </Link>

                <hr className="sidebar-divider my-0" />

                <div className="sidebar-card">
                    Hello, {user}
                </div>

            </ul>}</div>
        );
    }
}