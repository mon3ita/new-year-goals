<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use Redirect;
use Response;

use App\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['signin', 'signup']]);
    }

    public function signin(Request $request) {
        $validation = Validator::make($request->all(), [
            "email" => "required",
            "password" => "required"
        ]);

        if($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        $credentials = $request->only(['email', 'password']);

        if (! $token = JWTAuth::attempt($credentials)) {
            return response()->json([
                'error' => 'Wrong credential given'
            ]);
        }

        return response()->json([
            'data' => json_encode(
                array(
                    "token" => $token,
                    "name" => auth()->user()->name
                )
            )
        ]);
    }

    public function signup(Request $request) {
        $this->validate($request, [
            "name" => "required",
            "email" => "required",
            "password" => "required|min:8"
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        $response = array(
            "status" => 200,
            "user" => $user->email
        );

        return json_encode($response);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        auth()->logout();

        return response()->json(['message' => 'User successfully signed out']);
    }
    
    public function getAuthenticatedUser() {
                    
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }

        return response()->json(compact('user'));
    }
}
