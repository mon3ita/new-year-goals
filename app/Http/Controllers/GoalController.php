<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Validator;
use JWTAuth;

use App\Goal;

class GoalController extends Controller
{
    public function index() {
        $user = JWTAuth::parseToken()->authenticate();

        if (!$user) {
            return redirect("/signin");
        }

        $goals = DB::table('goals')->where('user_id', $user->id)->get();

        $response = array(
            "data" => $goals
        );

        return response()->json($response, 200);
    }
    
    public function create(Request $request) {
        $user = JWTAuth::parseToken()->authenticate();

        $validation = Validator::make($request->all(), [
            "title" => "required",
            "description" => "required",
            "importance" => "required",
        ]);

        if($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        $input = $request->only("title", "description", "importance");
        $goal = new Goal();
        $goal->title = $request->title;
        $goal->description = $request->description;
        $goal->importance = $request->importance;
        $goal->user_id = $user->id;
        $goal->save();

        $response = array(
            "status" => 200,
            "message" => "Created successfully"
        );
        return response()->json($response, 200);
    }

    public function get($id) {
        $user = JWTAuth::parseToken()->authenticate();

        $goal = DB::table('goals')->where([
            ['user_id', '=', $user->id],
            ['id', '=', $id]
        ])->get();

        if(!$goal) {
            $response = array(
                "message" => "Goal with given id doesn't exist."
            );
            return response()->json($response, 422);
        }

        return  response()->json($goal, 200);
    }

    public function update(Request $request) {
        $user = JWTAuth::parseToken()->authenticate();

        $validation = Validator::make($request->all(), [
            "title" => "required",
            "description" => "required",
            "importance" => "required",
            "achieved" => "required",
        ]);

        if($validation->fails()) {
            $response = array(
                "message" => "Goal with given id doesn't exist."
            );

            return response()->json($response, 422);
        }

        $input = $request->only("title", "description", "importance", "achieved");
        $goal = DB::table('goals')->where([
            ['id', '=', $request->id],
            ['user_id', '=', $user->id]
        ]);

        if(! $goal) {
            $response = array(
                "message" => "Goal with given id doesn't exist."
            );
            return response()->json($response, 422);
        }

        $goal->update($input);

        return response()->json($goal, 200);
    }

    public function delete(Request $request) {
        $user = JWTAuth::parseToken()->authenticate();

        $goal = DB::table('goals')
            ->where([
                ['id', '=', $request->id],
                ['user_id', '=', $user->id]
            ])
            ->delete();

        $goals = DB::table("goals")->get();
        $response = array(
            "message" => "Deleted successfully",
            "goals" => $goals
        );

        return response()->json($response, 200);
    }
}
