<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Goal extends Model
{
    protected $fillable = [
        "title", "description", "achieved", "importance", "created_at", "updated_at", "user_id"
    ];
}
